import random

def generate_city_description():
  # Liste von Namen für die Städte
  city_names = ["Arvandor", "Elysium", "Thule", "Zalthor", "Myridian"]
  # Liste von Beschreibungen für die Städte
  city_descriptions = [
    "eine mächtige Stadt, umgeben von hohen Mauern und bewacht von Elitekriegern",
    "eine friedliche Stadt im Herzen eines grünen Tales, bekannt für ihre prächtigen Gärten und Tempel",
    "eine mystische Stadt auf einem hohen Gipfel, umgeben von dichtem Nebel und geheimnisvollen Ruinen",
    "eine pulsierende Stadt am Meer, bekannt für ihre prächtigen Paläste und ihr reiches Handelsleben",
    "eine Stadt im Herzen des dunklen Waldes, bewohnt von geheimnisvollen Wesen und umgeben von geheimnisvollen Ruinen"
  ]
  # Liste von Rassen
  races = ["Menschen", "Elfen", "Zwerge", "Gnome", "Halblinge"]
  # Wähle zufällig Namen, Beschreibung, Anzahl der Einwohner und Rasse für jede Stadt aus
  for i in range(5):
    name = random.choice(city_names)
    description = random.choice(city_descriptions)
    population = random.randint(1000, 1000000) # Zufällige Anzahl von 1000 bis 1 Million Einwohner
    race = random.choice(races)
    print(f"{name} ist {description} und hat etwa {population} Einwohner, hauptsächlich {race}.")

generate_city_description()
